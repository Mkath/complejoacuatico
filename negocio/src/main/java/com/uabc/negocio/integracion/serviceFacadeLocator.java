package com.uabc.negocio.integracion;

import com.uabc.negocio.facade.administradorFacade; 

/**
 *
 * @author Carlos Arellano - Complejo Acuatico 2018
 */
public class serviceFacadeLocator {

    private static administradorFacade administradorFacade;  
    public static administradorFacade getInstanceAdministradorFacade() {
        if (administradorFacade == null) {
            administradorFacade = new administradorFacade();
            return administradorFacade;
        } else {
            return administradorFacade;
        }
    }  
 
}
