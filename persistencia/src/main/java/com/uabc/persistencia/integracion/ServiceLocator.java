package com.uabc.persistencia.integracion;

import com.uabc.persistencia.dao.adminDAO;

/**
 *
 * @author Carlos Arellano
 */
public class ServiceLocator {

    private static adminDAO adminDAO;

    public static adminDAO getInstanceAdminDAO() {
        if (adminDAO == null) {
            adminDAO = new adminDAO();
            return adminDAO;
        } else {
            return adminDAO;
        }
    }

}
