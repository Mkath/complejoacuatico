package com.uabc.persistencia.dao;

import com.uabc.entidad.Visitas;
import com.uabc.persistencia.persistencia.AbstractDAO;
import java.util.List;
/**
 *
 * @author Axel Valenzuela - Complejo Acuatico 2018
 */
public class visitasDAO extends AbstractDAO<Integer, Visitas> {

    public void addVisitas(Visitas visitas) {
        this.save(visitas);
    }

    public void updateVisitas(Visitas visitas) {
        this.update(visitas);
    }

    public List<Visitas> findAllVisitas() {
        return this.findAll();
    }

    public Visitas findByVisitasId(int id) {
        return this.find(id);
    }

    
}