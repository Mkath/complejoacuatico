package com.uabc.persistencia.dao;

import com.uabc.entidad.Credencialuabc;
import com.uabc.persistencia.persistencia.AbstractDAO;
import java.util.List;
/**
 *
 * @author Axel Valenzuela - Complejo Acuatico 2018
 */
public class credencialUabcDAO extends AbstractDAO<Integer, Credencialuabc> {

    /**
     *
     * @param uabc
     */
    public void addCredencialuabc(Credencialuabc uabc) {
        this.save(uabc);
    }

    /**
     *
     * @param uabc
     */
    public void updateCredencialuabc(Credencialuabc uabc) {
        this.update(uabc);
    } 

    /**
     *
     * @return
     */
    public List<Credencialuabc> findAllCredencialUabc() {
        return this.findAll();
    }

    /**
     *
     * @param id
     * @return
     */
    public Credencialuabc findByCredencialuabcId(int id) {
        return this.find(id);
    } 
    
}