package com.uabc.persistencia.dao;

import com.uabc.entidad.Credencialdetalle;
import com.uabc.persistencia.persistencia.AbstractDAO;
import java.util.List;
/**
 *
 * @author Axel Valenzuela - Complejo Acuatico 2018
 */
public class credencialDetalleDAO extends AbstractDAO<Integer, Credencialdetalle> {

    public void addCredencialDetalle(Credencialdetalle detalle) {
        this.save(detalle);
    }

    public void updateCredencialDetalle(Credencialdetalle detalle) {
        this.update(detalle);
    } 

    public List<Credencialdetalle> findAllCredencialDetalle() {
        return this.findAll();
    }

    public Credencialdetalle findByCredencialDetalleId(int id) {
        return this.find(id);
    } 
    
}