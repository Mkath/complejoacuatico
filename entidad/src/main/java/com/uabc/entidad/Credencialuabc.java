/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uabc.entidad;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author revy
 */
@Entity
@Table(name = "credencialuabc")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Credencialuabc.findAll", query = "SELECT c FROM Credencialuabc c")
    , @NamedQuery(name = "Credencialuabc.findByIdCredencialUabc", query = "SELECT c FROM Credencialuabc c WHERE c.idCredencialUabc = :idCredencialUabc")
    , @NamedQuery(name = "Credencialuabc.findByEgresado", query = "SELECT c FROM Credencialuabc c WHERE c.egresado = :egresado")
    , @NamedQuery(name = "Credencialuabc.findBySeguro", query = "SELECT c FROM Credencialuabc c WHERE c.seguro = :seguro")
    , @NamedQuery(name = "Credencialuabc.findByPoliza", query = "SELECT c FROM Credencialuabc c WHERE c.poliza = :poliza")
    , @NamedQuery(name = "Credencialuabc.findByTarifa", query = "SELECT c FROM Credencialuabc c WHERE c.tarifa = :tarifa")})
public class Credencialuabc implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idCredencialUabc")
    private Integer idCredencialUabc;
    @Basic(optional = false)
    @Column(name = "egresado")
    private String egresado;
    @Basic(optional = false)
    @Column(name = "seguro")
    private String seguro;
    @Basic(optional = false)
    @Column(name = "poliza")
    private String poliza;
    @Basic(optional = false)
    @Column(name = "tarifa")
    private int tarifa;
    @JoinColumn(name = "idCredencialDetalle", referencedColumnName = "idCredencialDetalle")
    @ManyToOne(optional = false)
    private Credencialdetalle idCredencialDetalle;

    public Credencialuabc() {
    }

    public Credencialuabc(Integer idCredencialUabc) {
        this.idCredencialUabc = idCredencialUabc;
    }

    public Credencialuabc(Integer idCredencialUabc, String egresado, String seguro, String poliza, int tarifa) {
        this.idCredencialUabc = idCredencialUabc;
        this.egresado = egresado;
        this.seguro = seguro;
        this.poliza = poliza;
        this.tarifa = tarifa;
    }

    public Integer getIdCredencialUabc() {
        return idCredencialUabc;
    }

    public void setIdCredencialUabc(Integer idCredencialUabc) {
        this.idCredencialUabc = idCredencialUabc;
    }

    public String getEgresado() {
        return egresado;
    }

    public void setEgresado(String egresado) {
        this.egresado = egresado;
    }

    public String getSeguro() {
        return seguro;
    }

    public void setSeguro(String seguro) {
        this.seguro = seguro;
    }

    public String getPoliza() {
        return poliza;
    }

    public void setPoliza(String poliza) {
        this.poliza = poliza;
    }

    public int getTarifa() {
        return tarifa;
    }

    public void setTarifa(int tarifa) {
        this.tarifa = tarifa;
    }

    public Credencialdetalle getIdCredencialDetalle() {
        return idCredencialDetalle;
    }

    public void setIdCredencialDetalle(Credencialdetalle idCredencialDetalle) {
        this.idCredencialDetalle = idCredencialDetalle;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCredencialUabc != null ? idCredencialUabc.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Credencialuabc)) {
            return false;
        }
        Credencialuabc other = (Credencialuabc) object;
        if ((this.idCredencialUabc == null && other.idCredencialUabc != null) || (this.idCredencialUabc != null && !this.idCredencialUabc.equals(other.idCredencialUabc))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uabc.entidad.Credencialuabc[ idCredencialUabc=" + idCredencialUabc + " ]";
    }
    
}
