/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uabc.entidad;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author revy
 */
@Entity
@Table(name = "visitas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Visitas.findAll", query = "SELECT v FROM Visitas v")
    , @NamedQuery(name = "Visitas.findByIdVisita", query = "SELECT v FROM Visitas v WHERE v.idVisita = :idVisita")
    , @NamedQuery(name = "Visitas.findByHoras", query = "SELECT v FROM Visitas v WHERE v.horas = :horas")
    , @NamedQuery(name = "Visitas.findByTurno", query = "SELECT v FROM Visitas v WHERE v.turno = :turno")
    , @NamedQuery(name = "Visitas.findByTipoPago", query = "SELECT v FROM Visitas v WHERE v.tipoPago = :tipoPago")
    , @NamedQuery(name = "Visitas.findByFecha", query = "SELECT v FROM Visitas v WHERE v.fecha = :fecha")})
public class Visitas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idVisita")
    private Integer idVisita;
    @Basic(optional = false)
    @Column(name = "horas")
    private int horas;
    @Basic(optional = false)
    @Column(name = "turno")
    private String turno;
    @Basic(optional = false)
    @Column(name = "tipoPago")
    private String tipoPago;
    @Basic(optional = false)
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @JoinColumn(name = "idCredencialDetalle", referencedColumnName = "idCredencialDetalle")
    @ManyToOne(optional = false)
    private Credencialdetalle idCredencialDetalle;

    public Visitas() {
    }

    public Visitas(Integer idVisita) {
        this.idVisita = idVisita;
    }

    public Visitas(Integer idVisita, int horas, String turno, String tipoPago, Date fecha) {
        this.idVisita = idVisita;
        this.horas = horas;
        this.turno = turno;
        this.tipoPago = tipoPago;
        this.fecha = fecha;
    }

    public Integer getIdVisita() {
        return idVisita;
    }

    public void setIdVisita(Integer idVisita) {
        this.idVisita = idVisita;
    }

    public int getHoras() {
        return horas;
    }

    public void setHoras(int horas) {
        this.horas = horas;
    }

    public String getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }

    public String getTipoPago() {
        return tipoPago;
    }

    public void setTipoPago(String tipoPago) {
        this.tipoPago = tipoPago;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Credencialdetalle getIdCredencialDetalle() {
        return idCredencialDetalle;
    }

    public void setIdCredencialDetalle(Credencialdetalle idCredencialDetalle) {
        this.idCredencialDetalle = idCredencialDetalle;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idVisita != null ? idVisita.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Visitas)) {
            return false;
        }
        Visitas other = (Visitas) object;
        if ((this.idVisita == null && other.idVisita != null) || (this.idVisita != null && !this.idVisita.equals(other.idVisita))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uabc.entidad.Visitas[ idVisita=" + idVisita + " ]";
    }
    
}
