/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uabc.entidad;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author revy
 */
@Entity
@Table(name = "administrador")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Administrador.findAll", query = "SELECT a FROM Administrador a")
    , @NamedQuery(name = "Administrador.findByIdAdministrador", query = "SELECT a FROM Administrador a WHERE a.idAdministrador = :idAdministrador")
    , @NamedQuery(name = "Administrador.findByNombreAdministrador", query = "SELECT a FROM Administrador a WHERE a.nombreAdministrador = :nombreAdministrador")
    , @NamedQuery(name = "Administrador.findByApellidoPaterno", query = "SELECT a FROM Administrador a WHERE a.apellidoPaterno = :apellidoPaterno")
    , @NamedQuery(name = "Administrador.findByApellidoMaterno", query = "SELECT a FROM Administrador a WHERE a.apellidoMaterno = :apellidoMaterno")
    , @NamedQuery(name = "Administrador.findByPuesto", query = "SELECT a FROM Administrador a WHERE a.puesto = :puesto")
    , @NamedQuery(name = "Administrador.findByCuentaAdministrador", query = "SELECT a FROM Administrador a WHERE a.cuentaAdministrador = :cuentaAdministrador")
    , @NamedQuery(name = "Administrador.findByContrase\u00f1aAdministador", query = "SELECT a FROM Administrador a WHERE a.contrase\u00f1aAdministador = :contrase\u00f1aAdministador")})
public class Administrador implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idAdministrador")
    private Integer idAdministrador;
    @Basic(optional = false)
    @Column(name = "nombreAdministrador")
    private String nombreAdministrador;
    @Basic(optional = false)
    @Column(name = "apellidoPaterno")
    private String apellidoPaterno;
    @Basic(optional = false)
    @Column(name = "apellidoMaterno")
    private String apellidoMaterno;
    @Basic(optional = false)
    @Column(name = "puesto")
    private String puesto;
    @Basic(optional = false)
    @Column(name = "cuentaAdministrador")
    private String cuentaAdministrador;
    @Basic(optional = false)
    @Column(name = "contrase\u00f1aAdministador")
    private String contraseñaAdministador;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idAdministrador")
    private List<Seguro> seguroList;

    public Administrador() {
    }

    public Administrador(Integer idAdministrador) {
        this.idAdministrador = idAdministrador;
    }

    public Administrador(Integer idAdministrador, String nombreAdministrador, String apellidoPaterno, String apellidoMaterno, String puesto, String cuentaAdministrador, String contraseñaAdministador) {
        this.idAdministrador = idAdministrador;
        this.nombreAdministrador = nombreAdministrador;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
        this.puesto = puesto;
        this.cuentaAdministrador = cuentaAdministrador;
        this.contraseñaAdministador = contraseñaAdministador;
    }

    public Integer getIdAdministrador() {
        return idAdministrador;
    }

    public void setIdAdministrador(Integer idAdministrador) {
        this.idAdministrador = idAdministrador;
    }

    public String getNombreAdministrador() {
        return nombreAdministrador;
    }

    public void setNombreAdministrador(String nombreAdministrador) {
        this.nombreAdministrador = nombreAdministrador;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public String getCuentaAdministrador() {
        return cuentaAdministrador;
    }

    public void setCuentaAdministrador(String cuentaAdministrador) {
        this.cuentaAdministrador = cuentaAdministrador;
    }

    public String getContraseñaAdministador() {
        return contraseñaAdministador;
    }

    public void setContraseñaAdministador(String contraseñaAdministador) {
        this.contraseñaAdministador = contraseñaAdministador;
    }

    @XmlTransient
    public List<Seguro> getSeguroList() {
        return seguroList;
    }

    public void setSeguroList(List<Seguro> seguroList) {
        this.seguroList = seguroList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAdministrador != null ? idAdministrador.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Administrador)) {
            return false;
        }
        Administrador other = (Administrador) object;
        if ((this.idAdministrador == null && other.idAdministrador != null) || (this.idAdministrador != null && !this.idAdministrador.equals(other.idAdministrador))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uabc.entidad.Administrador[ idAdministrador=" + idAdministrador + " ]";
    }
    
}
