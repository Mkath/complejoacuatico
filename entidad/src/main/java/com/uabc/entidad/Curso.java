/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uabc.entidad;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author revy
 */
@Entity
@Table(name = "curso")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Curso.findAll", query = "SELECT c FROM Curso c")
    , @NamedQuery(name = "Curso.findByIdCurso", query = "SELECT c FROM Curso c WHERE c.idCurso = :idCurso")
    , @NamedQuery(name = "Curso.findByNombreCurso", query = "SELECT c FROM Curso c WHERE c.nombreCurso = :nombreCurso")
    , @NamedQuery(name = "Curso.findByInstructor", query = "SELECT c FROM Curso c WHERE c.instructor = :instructor")
    , @NamedQuery(name = "Curso.findByDiasImpartidos", query = "SELECT c FROM Curso c WHERE c.diasImpartidos = :diasImpartidos")
    , @NamedQuery(name = "Curso.findByHorasCurso", query = "SELECT c FROM Curso c WHERE c.horasCurso = :horasCurso")})
public class Curso implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idCurso")
    private Integer idCurso;
    @Basic(optional = false)
    @Column(name = "nombreCurso")
    private String nombreCurso;
    @Basic(optional = false)
    @Column(name = "instructor")
    private String instructor;
    @Basic(optional = false)
    @Column(name = "diasImpartidos")
    private String diasImpartidos;
    @Basic(optional = false)
    @Column(name = "horasCurso")
    private String horasCurso;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCurso")
    private List<Credencialcurso> credencialcursoList;

    public Curso() {
    }

    public Curso(Integer idCurso) {
        this.idCurso = idCurso;
    }

    public Curso(Integer idCurso, String nombreCurso, String instructor, String diasImpartidos, String horasCurso) {
        this.idCurso = idCurso;
        this.nombreCurso = nombreCurso;
        this.instructor = instructor;
        this.diasImpartidos = diasImpartidos;
        this.horasCurso = horasCurso;
    }

    public Integer getIdCurso() {
        return idCurso;
    }

    public void setIdCurso(Integer idCurso) {
        this.idCurso = idCurso;
    }

    public String getNombreCurso() {
        return nombreCurso;
    }

    public void setNombreCurso(String nombreCurso) {
        this.nombreCurso = nombreCurso;
    }

    public String getInstructor() {
        return instructor;
    }

    public void setInstructor(String instructor) {
        this.instructor = instructor;
    }

    public String getDiasImpartidos() {
        return diasImpartidos;
    }

    public void setDiasImpartidos(String diasImpartidos) {
        this.diasImpartidos = diasImpartidos;
    }

    public String getHorasCurso() {
        return horasCurso;
    }

    public void setHorasCurso(String horasCurso) {
        this.horasCurso = horasCurso;
    }

    @XmlTransient
    public List<Credencialcurso> getCredencialcursoList() {
        return credencialcursoList;
    }

    public void setCredencialcursoList(List<Credencialcurso> credencialcursoList) {
        this.credencialcursoList = credencialcursoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCurso != null ? idCurso.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Curso)) {
            return false;
        }
        Curso other = (Curso) object;
        if ((this.idCurso == null && other.idCurso != null) || (this.idCurso != null && !this.idCurso.equals(other.idCurso))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uabc.entidad.Curso[ idCurso=" + idCurso + " ]";
    }
    
}
