/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uabc.entidad;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author revy
 */
@Entity
@Table(name = "credencialdetalle")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Credencialdetalle.findAll", query = "SELECT c FROM Credencialdetalle c")
    , @NamedQuery(name = "Credencialdetalle.findByIdCredencialDetalle", query = "SELECT c FROM Credencialdetalle c WHERE c.idCredencialDetalle = :idCredencialDetalle")
    , @NamedQuery(name = "Credencialdetalle.findByHrsDisponibles", query = "SELECT c FROM Credencialdetalle c WHERE c.hrsDisponibles = :hrsDisponibles")
    , @NamedQuery(name = "Credencialdetalle.findByFechaExpiracion", query = "SELECT c FROM Credencialdetalle c WHERE c.fechaExpiracion = :fechaExpiracion")
    , @NamedQuery(name = "Credencialdetalle.findByNombreVisitante", query = "SELECT c FROM Credencialdetalle c WHERE c.nombreVisitante = :nombreVisitante")
    , @NamedQuery(name = "Credencialdetalle.findByApellidoPaterno", query = "SELECT c FROM Credencialdetalle c WHERE c.apellidoPaterno = :apellidoPaterno")
    , @NamedQuery(name = "Credencialdetalle.findByApellidoMaterno", query = "SELECT c FROM Credencialdetalle c WHERE c.apellidoMaterno = :apellidoMaterno")
    , @NamedQuery(name = "Credencialdetalle.findByCorreo", query = "SELECT c FROM Credencialdetalle c WHERE c.correo = :correo")
    , @NamedQuery(name = "Credencialdetalle.findByNumeroEmergencia", query = "SELECT c FROM Credencialdetalle c WHERE c.numeroEmergencia = :numeroEmergencia")
    , @NamedQuery(name = "Credencialdetalle.findByNombreEmergencia", query = "SELECT c FROM Credencialdetalle c WHERE c.nombreEmergencia = :nombreEmergencia")
    , @NamedQuery(name = "Credencialdetalle.findByTipoCredencial", query = "SELECT c FROM Credencialdetalle c WHERE c.tipoCredencial = :tipoCredencial")
    , @NamedQuery(name = "Credencialdetalle.findByEstado", query = "SELECT c FROM Credencialdetalle c WHERE c.estado = :estado")
    , @NamedQuery(name = "Credencialdetalle.findBySticker", query = "SELECT c FROM Credencialdetalle c WHERE c.sticker = :sticker")})
public class Credencialdetalle implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idCredencialDetalle")
    private Integer idCredencialDetalle;
    @Basic(optional = false)
    @Column(name = "hrsDisponibles")
    private int hrsDisponibles;
    @Basic(optional = false)
    @Column(name = "fechaExpiracion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaExpiracion;
    @Basic(optional = false)
    @Column(name = "nombreVisitante")
    private String nombreVisitante;
    @Basic(optional = false)
    @Column(name = "apellidoPaterno")
    private String apellidoPaterno;
    @Basic(optional = false)
    @Column(name = "apellidoMaterno")
    private String apellidoMaterno;
    @Basic(optional = false)
    @Column(name = "correo")
    private String correo;
    @Basic(optional = false)
    @Column(name = "numeroEmergencia")
    private String numeroEmergencia;
    @Basic(optional = false)
    @Column(name = "nombreEmergencia")
    private String nombreEmergencia;
    @Basic(optional = false)
    @Column(name = "tipoCredencial")
    private String tipoCredencial;
    @Basic(optional = false)
    @Column(name = "estado")
    private String estado;
    @Basic(optional = false)
    @Column(name = "sticker")
    private int sticker;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCredencialDetalle")
    private List<Credencialcurso> credencialcursoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCredencialDetalle")
    private List<Credencialgeneral> credencialgeneralList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCredencialDetalle")
    private List<Credencialuabc> credencialuabcList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCredencialDetalle")
    private List<Visitas> visitasList;

    public Credencialdetalle() {
    }

    public Credencialdetalle(Integer idCredencialDetalle) {
        this.idCredencialDetalle = idCredencialDetalle;
    }

    public Credencialdetalle(Integer idCredencialDetalle, int hrsDisponibles, Date fechaExpiracion, String nombreVisitante, String apellidoPaterno, String apellidoMaterno, String correo, String numeroEmergencia, String nombreEmergencia, String tipoCredencial, String estado, int sticker) {
        this.idCredencialDetalle = idCredencialDetalle;
        this.hrsDisponibles = hrsDisponibles;
        this.fechaExpiracion = fechaExpiracion;
        this.nombreVisitante = nombreVisitante;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
        this.correo = correo;
        this.numeroEmergencia = numeroEmergencia;
        this.nombreEmergencia = nombreEmergencia;
        this.tipoCredencial = tipoCredencial;
        this.estado = estado;
        this.sticker = sticker;
    }

    public Integer getIdCredencialDetalle() {
        return idCredencialDetalle;
    }

    public void setIdCredencialDetalle(Integer idCredencialDetalle) {
        this.idCredencialDetalle = idCredencialDetalle;
    }

    public int getHrsDisponibles() {
        return hrsDisponibles;
    }

    public void setHrsDisponibles(int hrsDisponibles) {
        this.hrsDisponibles = hrsDisponibles;
    }

    public Date getFechaExpiracion() {
        return fechaExpiracion;
    }

    public void setFechaExpiracion(Date fechaExpiracion) {
        this.fechaExpiracion = fechaExpiracion;
    }

    public String getNombreVisitante() {
        return nombreVisitante;
    }

    public void setNombreVisitante(String nombreVisitante) {
        this.nombreVisitante = nombreVisitante;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getNumeroEmergencia() {
        return numeroEmergencia;
    }

    public void setNumeroEmergencia(String numeroEmergencia) {
        this.numeroEmergencia = numeroEmergencia;
    }

    public String getNombreEmergencia() {
        return nombreEmergencia;
    }

    public void setNombreEmergencia(String nombreEmergencia) {
        this.nombreEmergencia = nombreEmergencia;
    }

    public String getTipoCredencial() {
        return tipoCredencial;
    }

    public void setTipoCredencial(String tipoCredencial) {
        this.tipoCredencial = tipoCredencial;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public int getSticker() {
        return sticker;
    }

    public void setSticker(int sticker) {
        this.sticker = sticker;
    }

    @XmlTransient
    public List<Credencialcurso> getCredencialcursoList() {
        return credencialcursoList;
    }

    public void setCredencialcursoList(List<Credencialcurso> credencialcursoList) {
        this.credencialcursoList = credencialcursoList;
    }

    @XmlTransient
    public List<Credencialgeneral> getCredencialgeneralList() {
        return credencialgeneralList;
    }

    public void setCredencialgeneralList(List<Credencialgeneral> credencialgeneralList) {
        this.credencialgeneralList = credencialgeneralList;
    }

    @XmlTransient
    public List<Credencialuabc> getCredencialuabcList() {
        return credencialuabcList;
    }

    public void setCredencialuabcList(List<Credencialuabc> credencialuabcList) {
        this.credencialuabcList = credencialuabcList;
    }

    @XmlTransient
    public List<Visitas> getVisitasList() {
        return visitasList;
    }

    public void setVisitasList(List<Visitas> visitasList) {
        this.visitasList = visitasList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCredencialDetalle != null ? idCredencialDetalle.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Credencialdetalle)) {
            return false;
        }
        Credencialdetalle other = (Credencialdetalle) object;
        if ((this.idCredencialDetalle == null && other.idCredencialDetalle != null) || (this.idCredencialDetalle != null && !this.idCredencialDetalle.equals(other.idCredencialDetalle))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uabc.entidad.Credencialdetalle[ idCredencialDetalle=" + idCredencialDetalle + " ]";
    }
    
}
