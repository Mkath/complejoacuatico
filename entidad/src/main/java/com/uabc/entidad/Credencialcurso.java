/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uabc.entidad;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author revy
 */
@Entity
@Table(name = "credencialcurso")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Credencialcurso.findAll", query = "SELECT c FROM Credencialcurso c")
    , @NamedQuery(name = "Credencialcurso.findByIdCredencialCurso", query = "SELECT c FROM Credencialcurso c WHERE c.idCredencialCurso = :idCredencialCurso")
    , @NamedQuery(name = "Credencialcurso.findByPolizaSeguro", query = "SELECT c FROM Credencialcurso c WHERE c.polizaSeguro = :polizaSeguro")
    , @NamedQuery(name = "Credencialcurso.findByTarifa", query = "SELECT c FROM Credencialcurso c WHERE c.tarifa = :tarifa")})
public class Credencialcurso implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idCredencialCurso")
    private Integer idCredencialCurso;
    @Basic(optional = false)
    @Column(name = "polizaSeguro")
    private String polizaSeguro;
    @Basic(optional = false)
    @Column(name = "tarifa")
    private int tarifa;
    @JoinColumn(name = "idCredencialDetalle", referencedColumnName = "idCredencialDetalle")
    @ManyToOne(optional = false)
    private Credencialdetalle idCredencialDetalle;
    @JoinColumn(name = "idCurso", referencedColumnName = "idCurso")
    @ManyToOne(optional = false)
    private Curso idCurso;

    public Credencialcurso() {
    }

    public Credencialcurso(Integer idCredencialCurso) {
        this.idCredencialCurso = idCredencialCurso;
    }

    public Credencialcurso(Integer idCredencialCurso, String polizaSeguro, int tarifa) {
        this.idCredencialCurso = idCredencialCurso;
        this.polizaSeguro = polizaSeguro;
        this.tarifa = tarifa;
    }

    public Integer getIdCredencialCurso() {
        return idCredencialCurso;
    }

    public void setIdCredencialCurso(Integer idCredencialCurso) {
        this.idCredencialCurso = idCredencialCurso;
    }

    public String getPolizaSeguro() {
        return polizaSeguro;
    }

    public void setPolizaSeguro(String polizaSeguro) {
        this.polizaSeguro = polizaSeguro;
    }

    public int getTarifa() {
        return tarifa;
    }

    public void setTarifa(int tarifa) {
        this.tarifa = tarifa;
    }

    public Credencialdetalle getIdCredencialDetalle() {
        return idCredencialDetalle;
    }

    public void setIdCredencialDetalle(Credencialdetalle idCredencialDetalle) {
        this.idCredencialDetalle = idCredencialDetalle;
    }

    public Curso getIdCurso() {
        return idCurso;
    }

    public void setIdCurso(Curso idCurso) {
        this.idCurso = idCurso;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCredencialCurso != null ? idCredencialCurso.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Credencialcurso)) {
            return false;
        }
        Credencialcurso other = (Credencialcurso) object;
        if ((this.idCredencialCurso == null && other.idCredencialCurso != null) || (this.idCredencialCurso != null && !this.idCredencialCurso.equals(other.idCredencialCurso))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uabc.entidad.Credencialcurso[ idCredencialCurso=" + idCredencialCurso + " ]";
    }
    
}
