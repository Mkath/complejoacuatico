package com.uabc.entidad;

import com.uabc.entidad.Credencialcurso;
import com.uabc.entidad.Credencialgeneral;
import com.uabc.entidad.Credencialuabc;
import com.uabc.entidad.Visitas;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-12-16T17:22:55")
@StaticMetamodel(Credencialdetalle.class)
public class Credencialdetalle_ { 

    public static volatile SingularAttribute<Credencialdetalle, String> apellidoPaterno;
    public static volatile SingularAttribute<Credencialdetalle, String> estado;
    public static volatile SingularAttribute<Credencialdetalle, Integer> idCredencialDetalle;
    public static volatile SingularAttribute<Credencialdetalle, String> nombreEmergencia;
    public static volatile SingularAttribute<Credencialdetalle, Integer> hrsDisponibles;
    public static volatile SingularAttribute<Credencialdetalle, Integer> sticker;
    public static volatile SingularAttribute<Credencialdetalle, String> apellidoMaterno;
    public static volatile ListAttribute<Credencialdetalle, Visitas> visitasList;
    public static volatile SingularAttribute<Credencialdetalle, String> nombreVisitante;
    public static volatile SingularAttribute<Credencialdetalle, String> numeroEmergencia;
    public static volatile SingularAttribute<Credencialdetalle, String> tipoCredencial;
    public static volatile ListAttribute<Credencialdetalle, Credencialcurso> credencialcursoList;
    public static volatile SingularAttribute<Credencialdetalle, Date> fechaExpiracion;
    public static volatile SingularAttribute<Credencialdetalle, String> correo;
    public static volatile ListAttribute<Credencialdetalle, Credencialgeneral> credencialgeneralList;
    public static volatile ListAttribute<Credencialdetalle, Credencialuabc> credencialuabcList;

}