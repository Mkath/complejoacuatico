package com.uabc.entidad;

import com.uabc.entidad.Credencialcurso;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-12-16T17:22:55")
@StaticMetamodel(Curso.class)
public class Curso_ { 

    public static volatile SingularAttribute<Curso, String> diasImpartidos;
    public static volatile SingularAttribute<Curso, String> instructor;
    public static volatile ListAttribute<Curso, Credencialcurso> credencialcursoList;
    public static volatile SingularAttribute<Curso, String> horasCurso;
    public static volatile SingularAttribute<Curso, Integer> idCurso;
    public static volatile SingularAttribute<Curso, String> nombreCurso;

}