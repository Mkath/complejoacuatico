package com.uabc.entidad;

import com.uabc.entidad.Credencialdetalle;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-12-16T17:22:55")
@StaticMetamodel(Visitas.class)
public class Visitas_ { 

    public static volatile SingularAttribute<Visitas, Integer> horas;
    public static volatile SingularAttribute<Visitas, Date> fecha;
    public static volatile SingularAttribute<Visitas, Credencialdetalle> idCredencialDetalle;
    public static volatile SingularAttribute<Visitas, Integer> idVisita;
    public static volatile SingularAttribute<Visitas, String> tipoPago;
    public static volatile SingularAttribute<Visitas, String> turno;

}