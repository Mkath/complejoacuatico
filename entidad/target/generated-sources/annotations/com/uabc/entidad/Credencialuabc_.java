package com.uabc.entidad;

import com.uabc.entidad.Credencialdetalle;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-12-16T17:22:55")
@StaticMetamodel(Credencialuabc.class)
public class Credencialuabc_ { 

    public static volatile SingularAttribute<Credencialuabc, String> seguro;
    public static volatile SingularAttribute<Credencialuabc, Integer> tarifa;
    public static volatile SingularAttribute<Credencialuabc, Credencialdetalle> idCredencialDetalle;
    public static volatile SingularAttribute<Credencialuabc, String> egresado;
    public static volatile SingularAttribute<Credencialuabc, Integer> idCredencialUabc;
    public static volatile SingularAttribute<Credencialuabc, String> poliza;

}