package com.uabc.entidad;

import com.uabc.entidad.Credencialdetalle;
import com.uabc.entidad.Curso;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-12-16T17:22:55")
@StaticMetamodel(Credencialcurso.class)
public class Credencialcurso_ { 

    public static volatile SingularAttribute<Credencialcurso, Integer> tarifa;
    public static volatile SingularAttribute<Credencialcurso, Credencialdetalle> idCredencialDetalle;
    public static volatile SingularAttribute<Credencialcurso, Curso> idCurso;
    public static volatile SingularAttribute<Credencialcurso, String> polizaSeguro;
    public static volatile SingularAttribute<Credencialcurso, Integer> idCredencialCurso;

}